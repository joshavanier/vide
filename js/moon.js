/**
 * Vide
 * A worldbuilding tool
 *
 * @author Josh Avanier
 * @version 0.1.0
 * @license MIT
 */

Vide = window.Vide || {}
Vide.moon = {

  /**
   * Calculate lunar mass
   * @param {number} r - Lunar radius
   * @param {number} p - Lunar density
   * @returns {number} Lunar mass
   */

  mass(r, p) {
    return Math.pow(r, 3) * p
  },

  /**
   * Calculate lunar surface gravity
   * @param {number} m - Lunar mass
   * @param {number} r - Lunar radius
   * @returns {number} Lunar surface gravity
   */

  surfaceGrav(m, r) {
    return m / Math.pow(r, 2)
  },

  /**
   * Calculate orbital period
   * @param {number} o - Lunar orbit radius
   * @param {number} p - Planetary mass
   * @param {number} m - Lunar mass
   * @returns {number} Orbital period
   */

  orbitalPeriod(o, p, m) {
    return 0.0588 * Math.sqrt(Math.pow(o, 3) / (p + m))
  }
}
