/**
 * Vide
 * A worldbuilding tool
 *
 * @author Josh Avanier
 * @version 0.1.0
 * @license MIT
 */

const solLumWatt = 3.828E26
const solLumLum = 3.75E28
const solMass = 1.988435E30
const solRadius = 695700 // km

Vide = window.Vide || {}
Vide.star = {

  data: {

    /*
      b: B-V
      t: effective temperature
      av: absolute magnitude visual
      ab: absolute magnitude bolometric
      c: bolometric correction
      l: luminosity
      r: radius
      m: mass
      p: density
      f: lifetime
    */

    O5:{b:-.35,t:4E4,av:-5.8,ab:-10,c:-4.2,l:81E4,r:18.7,m:35.9,p:.008,f:44E4},
    B0:{b:-.3,t:3E4,av:-4.3,ab:-7.2,c:-2.9,l:61E3,r:9.2,m:18.2,p:.033,f:3E6},
    B5:{b:-.16,t:16E3,av:-1.1,ab:-2.5,c:-1.5,l:810,r:3.7,m:5.8,p:.16,f:72E6},
    A0:{b:0,t:1E4,av:.8,ab:.3,c:-.5,l:64,r:2.7,m:3,p:.22,f:47E7},
    A2:{b:.06,t:9700,av:1.2,ab:.9,c:-.3,l:35,r:2.1,m:2.6,p:.39,f:72E7},
    A5:{b:.14,t:8600,av:1.9,ab:1.6,c:-.3,l:19,r:1.94,m:2.16,p:.42,f:12E8},
    A7:{b:.2,t:8100,av:2.1,ab:1.9,c:-.2,l:14,r:1.9,m:2.01,p:.41,f:14E8},
    F0:{b:.31,t:7300,av:2.6,ab:2.5,c:-.1,l:8.5,r:1.82,m:1.75,p:.41,f:21E8},
    F2:{b:.38,t:6900,av:2.9,ab:2.9,c:0,l:5.6,r:1.65,m:1.57,p:.49,f:28E8},
    F5:{b:.44,t:6600,av:3.3,ab:3.3,c:0,l:3.9,r:1.5,m:1.43,p:.59,f:37E8},
    F7:{b:.5,t:6300,av:3.8,ab:3.8,c:0,l:2.4,r:1.31,m:1.27,p:.79,f:52E8},
    G0:{b:.59,t:6E3,av:4.4,ab:4.4,c:0,l:1.4,r:1.1,m:1.09,p:1.17,f:78E8},
    G2:{b:.64,t:5770,av:4.7,ab:4.7,c:0,l:1.07,r:1.03,m:1.02,p:1.3,f:95E8},
    G5:{b:.69,t:5600,av:5.1,ab:5,c:-.1,l:.81,r:.95,m:.95,p:1.53,f:12E9},
    G8:{b:.72,t:5400,av:5.5,ab:5.3,c:-.2,l:.61,r:.89,m:.88,p:1.73,f:14E9},
    K0:{b:.84,t:5200,av:5.9,ab:5.8,c:-.2,l:.29,r:.78,m:.79,p:2.31,f:25E9},
    K2:{b:.92,t:4800,av:6.3,ab:6.1,c:-.2,l:.29,r:.78,m:.72,p:2.13,f:25E9},
    K5:{b:1.17,t:4400,av:7.4,ab:6.6,c:-.8,l:.19,r:.74,m:.64,p:2.23,f:35E9},
    K7:{b:1.34,t:4200,av:8.1,ab:7.2,c:-.9,l:.11,r:.62,m:.55,p:3.34,f:52E9},
    M0:{b:1.43,t:3900,av:8.8,ab:7.8,c:-1,l:.061,r:.54,m:.48,p:4.24,f:78E9},
    M2:{b:1.52,t:3500,av:10.1,ab:8.3,c:-1.8,l:.039,r:.53,m:.42,p:3.92,f:11E10},
    M4:{b:1.56,t:3200,av:11.1,ab:8.8,c:-2.3,l:.024,r:.51,m:.38,p:4.04,f:15E10},
    M6:{b:1.62,t:2900,av:12.1,ab:9.5,c:-2.6,l:.013,r:.45,m:.32,p:4.97,f:25E10},
    M8:{b:1.9,t:2500,av:16,ab:11.8,c:-4.2,l:.0015,r:.21,m:.18,p:28.03,f:12E11}
  },

  /**
   * Calculate absolute magnitude
   * @param {number} m - Apparent magnitude
   * @param {number} p - Distance from Sol in parsecs
   * @returns {number} Absolute magnitude
   */

  absoluteMagnitude(m, p) {
    return m + 5 - (5 * Math.log(p))
  },

  /**
   * Calculate bolometric absolute magnitude
   * @param {number} a - Absolute magnitude
   * @param {number} b - Bolometric correction
   * @returns {number} Bolometric absolute magnitude
   */

  bolometricAbsoluteMagnitude(a, b) {
    return a + b
  },

  /**
   * Calculate bolometric luminosity
   * @param {number} b - Bolometric absolute magnitude
   * @returns {number} Bolometric luminosity
   */

  bolometricLuminosity(b) {
    return Math.pow(2.52, (4.85 - b))
  },

  /**
   * Calculate luminosity (watts)
   * @param {number} l - Luminosity
   * @returns {number} Luminosity (watts)
   */

  luminosityW(l) {
    return l * solLumWatt
  },

  /**
   * Calculate luminosity (lumens)
   * @param {number} l - Luminosity
   * @returns {number} Luminosity (lumens)
   */

  luminosityLm(l) {
    return l * solLumLum
  },

  /**
   * Calculate stellar mass
   * @param {number} l - Bolometric luminosity
   * @returns {number} Stellar mass
   */

  mass(l) {
    return Math.pow(l, 0.2632)
  },

  /**
   * Calculate mass (kg)
   * @param {number} m - Mass
   * @returns {number} Mass (kg)
   */

  massKg(m) {
    return m * solMass
  },

  /**
   * Calculate stellar diameter
   * @param {number} t - Effective temperature (Kelvin)
   * @param {number} l - Bolometric luminosity
   * @returns {number} Stellar diameter
   */

  diameter(t, l) {
    return Math.pow(5770, 2) / Math.pow(t, 2) * Math.sqrt(l)
  },

  /**
   * Calculate stellar diameter (km)
   * @param {number} d - Stellar diameter
   * @returns {number} Stellar diameter (km)
   */

  diameterKm(d) {
    return d * 1391600
  },

  /**
   * Calculate radius (km)
   * @param {number} r - Radius
   * @returns {number} Radius (km)
   */

  radiusKm(r) {
    return r * solRadius
  }
}
