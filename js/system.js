/**
 * Vide
 * A worldbuilding tool
 *
 * @author Josh Avanier
 * @version 0.1.0
 * @license MIT
 */

Vide = window.Vide || {}
Vide.system = {

  /**
   * Calculate system inner limit (gravity)
   * @param {number} m - Stellar mass
   * @returns {number} System inner limit (gravity) (AU)
   */

  innerGrav(m){
    return 0.2 * m
  },

  /**
   * Calculate system outer limit (gravity)
   * @param {number} m - Stellar mass
   * @returns {number} System outer limit (gravity) (AU)
   */

  outerGrav(m){
    return 40 * m
  },

  /**
   * Calculate system inner limit (sunlight)
   * @param {number} l - Bolometric luminosity
   * @returns {number} System inner limit (sunlight) (AU)
   */

  innerLight(l) {
    return Math.sqrt(l / 16)
  },

  /**
   * Calculate circumstellar habitable zone inner limit
   * @param {number} l - Bolometric luminosity
   * @returns {number} Circumstellar habitable zone inner limit (AU)
   */

  habZoneInner(l) {
    return Math.sqrt(l / 1.1)
  },

  /**
   * Calculate circumstellar habitable zone outer limit
   * @param {number} l - Bolometric luminosity
   * @returns {number} Circumstellar habitable zone outer limit (AU)
   */

  habZoneOuter(l) {
    return Math.sqrt(l / 0.53)
  },

  /**
   * Calculate frost line
   * @param {number} l - Bolometric luminosity
   * @returns {number} Frost line (AU)
   */

  frostline(l) {
    return Math.sqrt(l / 0.04)
  },

  /**
   * Calculate liquid hydrogen line
   * @param {number} l - Bolometric luminosity
   * @returns {number} Liquid hydrogen line (AU)
   */

  lh2Line(l) {
    return Math.sqrt(l / 0.0025)
  }
}
