/**
 * Vide
 * A worldbuilding tool
 *
 * @author Josh Avanier
 * @version 0.1.0
 * @license MIT
 */

const AU = 149597870700

var Vide = {

  /**
   * Open a tab
   */

  tab(s) {
    let x = document.getElementsByClassName('sect')
    let b = document.getElementsByClassName('tab')

    for (let i = 0, l = x.length; i < l; i++) {
      x[i].style.display = 'none'
    }

    for (let i = 0, l = b.length; i < l; i++) {
      b[i].className = 'tab on bg-cl o5 mr3'
    }

    document.getElementById(s).style.display = 'block'
    document.getElementById(`b-${s}`).className = 'tab on bg-cl of mr3'
  },

  update() {
    let s = (e, c) => {
      document.getElementById(e).innerHTML = c
    }
    let v = e => document.getElementById(e).value
    let compare = (a, b) => a > b ? a : b

    let sun = ' \u2609'
    let terra = ' \u2295'
    let spectralClass = v('setSpectralClass')
    let star = Vide.star.data[spectralClass]
    let lumBol = Vide.star.bolometricLuminosity(star.ab)
    let stellarDiameter = Vide.star.diameter(star.t, lumBol)
    let pMass = Number(v('setPlanetaryMass'))
    let pRadius = v('setPlanetaryRadius')
    let pDistance = v('setDistanceFromStar')
    let pEccentricity = v('setOrbitEccentricity')
    let pTilt = v('setAxialTilt')
    let sunlightIntensity = v('setSunlightIntensity')
    let greenhouse = v('setGreenhouseFudgeFactor')
    let albedo = v('setAlbedo')
    let observer = v('setObserverHeight')
    let pMeanDensity = Vide.planet.density(pRadius, pMass)
    let temp = Vide.planet.temperature(greenhouse, albedo, sunlightIntensity)
    let year = Vide.planet.planetaryYear(pDistance, star.m)
    let lRadius = v('setLunarRadius')
    let lDensity = v('setLunarDensity')
    let lOrbitalDistance = Number(v('setLunarOrbitalDistance'))
    let lMass = Vide.moon.mass(lRadius, lDensity)
    let orbp = Vide.moon.orbitalPeriod(lOrbitalDistance, pMass, lMass)
    let orbitalVel = Vide.planet.orbitalVelocity(star.m, pDistance)

    s('bvColourIndex', star.b)
    s('effectiveTemp', `${star.t} K`)
    s('absMagVis', star.av)
    s('absMagBol', star.ab)
    s('bolCor', star.c)
    s('stellarLuminosity', `${star.l} L${sun}`)
    s('stellarDiameter', `${stellarDiameter.toFixed(2)} ${sun}`)
    s('stellarDiameterKm', `${Vide.star.diameterKm(stellarDiameter).toFixed(2)} km`)
    s('stellarRadius', `${star.r} R${sun}`)
    s('stellarRadiusKm', `${Vide.star.radiusKm(star.r)} km`)
    s('stellarMass', `${star.m} M${sun}`)
    s('stellarMassKg', `${Vide.star.massKg(star.m).toFixed(2)} kg`)
    s('stellarDensity', `${star.p} g/cm\u00B3`)
    s('stellarLifetime', `${star.f.toExponential()} y`)

    s('sysInnGrav', `${compare(Vide.system.innerGrav(star.m), Vide.system.innerLight(lumBol)).toFixed(2)} AU`)
    s('sysOutGrav', `${Vide.system.outerGrav(star.m).toFixed(2)} AU`)
    s('CHZIL', `${Vide.system.habZoneInner(lumBol).toFixed(2)} AU`)
    s('CHZOL', `${Vide.system.habZoneOuter(lumBol).toFixed(2)} AU`)
    s('frostLine', `${Vide.system.frostline(lumBol).toFixed(2)} AU`)
    s('LH2Line', `${Vide.system.lh2Line(lumBol).toFixed(2)} AU`)

    s('planetaryMass', `${pMass} M${terra}`)
    s('planetaryMassKg', `${Vide.planet.massKg(pMass).toFixed(2)} kg`)
    s('planetaryRadius', `${pRadius} ${terra}`)
    s('planetaryRadiusKm', `${Vide.planet.radiusKm(pRadius).toFixed(2)} km`)
    s('planetaryDensity', `${pMeanDensity.toFixed(2)} ${terra}`)
    s('planetaryTemp', `${temp.toFixed(2)} K`)
    s('planetaryTempCelsius', `${Vide.planet.temperatureCelsius(temp).toFixed(2)}\u00B0C`)
    s('greenhouseFudgeFactor', greenhouse)
    s('sunlightIntensity', sunlightIntensity)
    s('escapeVelocity', `${Vide.planet.escapeVelocity(Vide.planet.radiusM(pRadius), Vide.planet.densityKgm(pMeanDensity)).toFixed(2)} m/s`)
    s('distanceToHorizon', `${Vide.planet.distanceToHorizon(Vide.planet.radiusM(pRadius), Number(observer)).toFixed(2)} m`)
    s('axialTilt', `${pTilt}\u00B0`)
    s('polarCircles', `${90 - pTilt}\u00B0 N/S`)
    s('tropics', `${pTilt}\u00B0 N/S`)
    s('distanceFromStar', `${Vide.planet.distanceFromStar(lumBol, sunlightIntensity).toFixed(2)} AU`)
    s('planetaryGrav', `${Vide.planet.accelerationG(pMass, pRadius).toFixed(2)} g`)
    s('planetaryYear', `${year.toFixed(2)} ${terra} y`)
    s('planetaryDay', `${Vide.planet.planetaryDay(year).toFixed(2)} ${terra} h`)
    s('periapsis', `${Vide.planet.periapsis(pDistance, pEccentricity).toFixed(2)} AU`)
    s('apoapsis', `${Vide.planet.apoapsis(pDistance, pEccentricity).toFixed(2)} AU`)
    s('planetaryOrbitalPeriod', `${Vide.planet.orbitalPeriod(pDistance, star.m).toFixed(2)} ${terra} y`)
    s('planetaryOrbitalVelocity', `${orbitalVel.toFixed(2)} ${terra}`)
    s('planetaryOrbitalVelocityKm', `${Vide.planet.orbitalVelocityKm(orbitalVel).toFixed(2)} km/s`)
    s('planetaryRocheLimit', `${Vide.planet.rocheLimit(pDistance, pMass, star.m).toFixed(2)} R${terra}`)
    s('planetaryHillSphere', `${Vide.planet.hillSphere(pRadius, pMeanDensity, lDensity).toFixed(2)} R${terra}`)

    s('lunarMass', `${lMass.toFixed(2)} M${terra}`)
    s('lunarRadius', `${lRadius} R${terra}`)
    s('lunarDensity', `${lDensity} g/cm\u00B3`)
    s('lunarSurfaceGravity', `${Vide.moon.surfaceGrav(lMass, lRadius).toFixed(2)} ${terra}`)
    s('lunarOrbitalPeriod', `${orbp.toFixed(2)} ${terra} d`)
    s('lunarPhaseLength', `${(orbp / 4).toFixed(2)} ${terra} d`)
  },

  init() {
    let set = e => {
      if (localStorage.hasOwnProperty(e))
        document.getElementById(e).value = JSON.parse(localStorage.getItem(e))

      document.getElementById(e).addEventListener('change', function() {
        localStorage.setItem(e, JSON.stringify(document.getElementById(e).value))
      })
    }

    set('setSpectralClass')
    set('setPlanetaryMass')
    set('setPlanetaryRadius')
    set('setDistanceFromStar')
    set('setSunlightIntensity')
    set('setGreenhouseFudgeFactor')
    set('setAlbedo')
    set('setObserverHeight')
    set('setOrbitEccentricity')
    set('setAxialTilt')
    set('setLunarRadius')
    set('setLunarDensity')
    set('setLunarOrbitalDistance')

    Vide.update()
  }
}
