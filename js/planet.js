/**
 * Vide
 * A worldbuilding tool
 *
 * @author Josh Avanier
 * @version 0.1.0
 * @license MIT
 */

const terraMass = 5.9721986E24 // kg
const terraRadius = 6371.008 // km
const terraRadiusM = 6371008 // m
const terraCircumference = 40075 // km
const terraOrbitKm = 29.8 // km

Vide = window.Vide || {}
Vide.planet = {

  /**
   * Calculate distance of planet from primary star
   * @param {number} l - Bolometric luminosity
   * @param {number} s - Sunlight intensity
   * @returns {number} Distance of planet from primary star (AU)
   */

  distanceFromStar(l, s) {
    return Math.sqrt(l / s)
  },

  /**
   * Calculate planetary year
   * @param {number} d - Distance of planet from primary star (AU)
   * @param {number} m - Mass of primary star
   * @returns {number} Planetary year
   */

  planetaryYear(d, m) {
    return Math.sqrt(Math.pow(d, 3) / m)
  },

  /**
   * Calculate planetary day (Earth hours)
   * @param {number} y - Planetary year
   * @returns {number} Planetary day (Earth hours)
   */

  planetaryDay(y) {
    return ((y * 365.25) / 364) * 23.934472
  },

  /**
   * Calculate angular diameter of primary star (apparent size)
   * @param {number} dia - Diameter of primary star (km)
   * @param {number} dis - Distance of planet from primary star (km)
   * @returns {number} Angular diameter of primary star
   */

  angularDiameter(dia, dis) {
    return 57.3 * (dia / dis)
  },

  /**
   * Calculate acceleration due to gravity (g)
   * @param {number} m - Planetary mass
   * @param {number} r - Planetary radius
   * @returns {number} Acceleration due to gravity (g)
   */

  accelerationG(m, r) {
    return m / Math.pow(r, 2)
  },

  /**
   * Calculate acceleration due to gravity (m/s^2)
   * @param {number} g - Acceleration due to gravity (g)
   * @returns {number} Acceleration due to gravity (m/s^2)
   */

  accelerationMS(g) {
    return g * 9.81
  },

  /**
   * Calculate planetary mass
   * @param {number} r - Planetary radius
   * @param {number} p - Planetary mean density
   * @returns {number} Planetary mass
   */

  mass(r, p) {
    return Math.pow(r, 3) / p
  },

  /**
   * Calculate planetary mass (kg)
   * @param {number} m - Planetary mass
   * @returns {number} Planetary mass (kg)
   */

  massKg(m) {
    return m * terraMass
  },

  /**
   * Calculate planetary density
   * @param {number} r - Planetary radius
   * @param {number} m - Planetary mass
   * @returns {number} Planetary density
   */

  density(r, m) {
    return Math.pow(r, 3) / m
  },

  /**
   * Calculate planetary density (kg/m)
   * @param {number} p - Planetary density
   * @returns {number} Planetary density (kg/m)
   */

  densityKgm(p) {
    return p * 5500
  },

  /**
   * Calculate planetary radius
   * @param {number} m - Planetary mass
   * @param {number} p - Planetary mean density
   * @return {number} Planetary radius
   */

  radius(m, p) {
    return Math.cbrt(m * p)
  },

  /**
   * Calculate planetary radius (km)
   * @param {number} r - Planetary radius
   * @returns {number} Planetary radius (km)
   */

  radiusKm(r) {
    return r * terraRadius
  },

  /**
   * Calculate planetary radius (m)
   * @param {number} r - Planetary radius
   * @returns {number} Planetary radius (m)
   */

  radiusM(r) {
    return r * terraRadiusM
  },

  /**
   * Calculate planetary circumference
   * @param {number} r - Planetary radius
   * @returns {number} Planetary circumference
   */

  circumference(r) {
    return 2 * Math.PI * r
  },

  /**
   * Calculate planetary temperature
   * @param {number} g - Greenhouse fudge factor
   * @param {number} a - Bond albedo
   * @param {number} s - Sunlight intensity
   */

  temperature(g, a, s) {
    return 374 * g * (1 - a) * Math.pow(s, 0.25)
  },

  /**
   * Convert to Celsius
   * @param {number} t - Temperature (K)
   * @returns {number} Celsius temperature
   */

  temperatureCelsius(t) {
    return t - 273.15
  },

  /**
   * Calculate escape velocity (m/s)
   * @param {number} r - Planetary radius (m)
   * @param {number} p - Planetary mean density (kg/m^2)
   * @returns {number} Escape velocity (m/s)
   */

  escapeVelocity(r, p) {
    return (2.365 * Math.pow(10, -5)) * r * Math.sqrt(p)
  },

  /**
   * Calculate distance to horizon
   * @param {number} r - Planetary radius (m)
   * @param {number} h - Height of observer's eyes above planetary surface
   * @returns {number} Distance to horizon (m)
   */

  distanceToHorizon(r, h) {
    return Math.sqrt(Math.pow((r + h), 2) - Math.pow(r, 2))
  },

  /**
   * Calculate periapsis
   * @param {number} a - Semi-major axis
   * @param {number} e - Planet's eccentricity
   * @returns {number} Periapsis
   */

  periapsis(a, e) {
    return a * (1 - e)
  },

  /**
   * Calculate apoapsis
   * @param {number} a - Semi-major axis
   * @param {number} e - Planet's eccentricity
   * @returns {number} Apoapsis
   */

  apoapsis(a, e) {
    return a * (1 + e)
  },

  /**
   * Calculate orbital period
   * @param {number} a - Semi-major axis
   * @param {number} m - Solar mass
   * @returns {number} Orbital period (Earth years)
   */

  orbitalPeriod(a, m) {
    return Math.sqrt(Math.pow(a, 3) / m)
  },

  /**
   * Calculate orbital velocity
   * @param {number} m - Solar mass
   * @param {number} r - Semi-major axis
   * @returns {number} Orbital velocity
   */

  orbitalVelocity(m, r) {
    return Math.sqrt(m / r)
  },

  /**
   * Calculate orbital velocity (km)
   * @param {number} o - Orbital velocity
   * @returns {number} Orbital velocity (km)
   */

  orbitalVelocityKm(o) {
    return o * terraOrbitKm
  },

  /**
   * Calculate hill sphere
   * @param {number} a - Semi-major axis
   * @param {number} m - Planetary mass (Earth masses)
   * @param {number} s - Solar mass
   * @returns {number} Hill sphere
   */

  hillSphere(a, m, s) {
    return a * Math.cbrt(m / s) * 235
  },

  /**
   * Calculate Roche limit
   * @param {number} r - Planetary radius
   * @param {number} p - Planetary density
   * @param {number} s - Lunar density
   * @returns {number} Roche limit
   */

  rocheLimit(r, p, s) {
    return 2.44 * r * Math.pow((p / s), (1 / 3))
  }

}
