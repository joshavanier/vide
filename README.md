![MIT](https://joshavanier.github.io/badges/svg/mit.svg)

**Vide** is a simple worldbuilding tool available as an Electron app for Linux, macOS, and Windows.

## To-do
- Add ability to import and export worldbuild data

## Development

```
npm install
npm start
```

---

Josh Avanier

[![@joshavanier](https://joshavanier.github.io/badges/svg/twitter.svg)](https://twitter.com/joshavanier) [![joshavanier.com](https://joshavanier.github.io/badges/svg/website.svg)](https://joshavanier.com)
